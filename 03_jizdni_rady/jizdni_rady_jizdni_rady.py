'''
Navrhnete strukturu a funkcionalitu pro system udrzujici jizdni rady
Pole (list) bude predstavovat (databazi jizdnich radu) a jizdni rady budou představeny
slovnikem, ktery bude obsahovat nasledujici vlastnosti:
  ID trasy: integer, např. 1. Každé trase vyberte identické číslo
  misto odjezdu: nazev mesta
  misto prijezdu: nazev jineho mesta
  cas odjezdu: ve formatu HH:mm (napr. 18:00) typu retezec ('18:00'). Kdo chce muze vyuzit datovy typ datetime (https://docs.python.org/3/library/datetime.html)
  cas prijezdu: obdobne jako cas odjezdu
  typ dopravního prostředku: řetězec
  vzdalenost: vzdalenost mezi mesty v km
  kilometrovou sazbu: cislo typu float (napr. 1.50)

Celý slovník představující linku by mohl vypadat nějak takto:
{'id': 1, 'odjezd_mesto': 'Brno', 'prijezd_mesto': 'Praha', 'odjezd_cas': '7:00', 'prijezd_cas': '9:30', 'prostredek': 'autobus', 'vzdalenost': 200, 'sazba': 1.50}

Systém bude umět (znamená definujte funkce, které budou dělat) následující:
1. Zadat do systému jednu novou trasu - pokud uzivatel zada trasu se stejným ID, vypište hlášku, že zadaná trasa již existuje trasu do pole nepřidejte
2. Zadat do systému více nových tras najednou
3. Vypsat vsechny informace o trase, podle ID
4. Vypište trasy, které jsou obsluhovány dopravním prostředkem, který uživatel zadá
5. Vypočítejte cenu cesty, kterou uzivatel bude chtit spocitat (podle ID). Použijte vzdalenost a kilometrovou sazbu
6. Vymazani trasy podle zadaneho ID. Uzivatel napriklad zada ze chce vymazat trasu s ID 5 a vy danou trasu vymazete
7. Vypocitejte kolik minut dana linka jede. Najdete funkci, ktera vam cas rozdeli podle dvojtecky cimz ziskate hodiny a minuty zvlast. Z tech se pokuste vypocitat cas cesty v minutach

Na úvod si v kódu systém naplňte několika trasama ať je nemusíte vždycky vyplňovat ručně
Celému návrhu ponechávám volnou ruku. Je na vás jak navrhnete a naprogramujete systém, ale pokuste se ho nejak naprogramovat
Správnost řešení pak můžeme probrat osobně

PORADNE SI PRECTETE ZADANI A PROMYSLETE HO


Pokud si s něčím nebudete ani po týdenním zoufalství vůbec vědět rady, tak udělejte aspoň to co znáte a na zbytek se klidně zeptejte přes mail nebo osobně na příští hodině.
'''

lines = [{'id': 1, 'from_city': 'Brno', 'to_city': 'Praha', 'departure': '8:00',
          'arrival': '10:30', 'type': 'bus', 'distance': 200, 'price_per_km': 1.50},
         {'id': 2, 'from_city': 'Praha', 'to_city': 'Brno', 'departure': '8:00',
          'arrival': '10:30', 'type': 'train', 'distance': 250, 'price_per_km': 1.75},
         {'id': 3, 'from_city': 'Brno', 'to_city': 'Olomouc', 'departure': '13:00',
          'arrival': '14:30', 'type': 'bus', 'distance': 100, 'price_per_km': 1.20},
         {'id': 4, 'from_city': 'Brno', 'to_city': 'Ostrava', 'departure': '15:10',
          'arrival': '17:01', 'type': 'bus', 'distance': 200, 'price_per_km': 1.45},
         {'id': 5, 'from_city': 'Znojmo', 'to_city': 'Brno', 'departure': '6:00',
          'arrival': '7:30', 'type': 'train', 'distance': 80, 'price_per_km': 1.15}]


def lines_ids():
    ids = []
    for line in lines:
        if isinstance(line, dict):
            ids.append(line['id'])
    return ids


# 1 Pridej jednu novou trasu
def add_line():
    input_id = input('Zadejte ID nové trasy: ')
    while int(input_id) in lines_ids():
        input_id = input('ID se již nachází v seznamu linek, zvolte jiné.')

    new_line = {
        'id': int(input_id),
        'from_city': input('Zadejte začátek trasy: '),
        'to_city': input('Zadejte konec trasy: '),
        'departure': input('Zadejte čas odjezdu: '),
        'arrival': input('Zadejte čas příjezdu: '),
        'type': input('Zadejte typ linky (bus, vlak...): '),
        'distance': input('Zadejte vzdálenost: '),
        'price_per_km': input('Zadejte cenu za km: ')}

    lines.append(new_line)
    print('Nová linka byla přidána:')
    print(new_line)


# 2 Pridej vice novych tras
def add_more_lines():
    add_line()
    end = input('Přidat další trasu? Zadej 1 pro ano, 0 pro ne')
    while end not in ['0', '1']:
        end = input('Zadej 0 pro konec nebo 1 pro přidání další linky')
    if end != '0':
        add_more_lines()


# 3 Zobraz trasu podle ID
def print_line_info(line_id):
    for line in lines:
        if isinstance(line, dict):
            if line['id'] == line_id:
                print(line)


# 4 Vypis trasy obsluhovane danym dopravnim prostredkem
def print_lines_by_type():
    transport_type = input('Zadejte typ prostředku:')
    filtered = []
    for line in lines:
        if isinstance(line, dict):
            if line['type'] == transport_type:
                filtered.append(line)
    print(filtered)


# 5 Vypocitej cenu cesty podle trasy
def count_total_price(line_id):
    for line in lines:
        if isinstance(line, dict):
            if line['id'] == line_id:
                print(line['distance'] * line['price_per_km'])


# 6 Smaz vybranou linku
def delete_line(line_id):
    for line in lines:
        if isinstance(line, dict):
            if line['id'] == line_id:
                lines.remove(line)
                print(lines)


# 7 Spocitej delku cesty
def count_travel_time(line_id):
    for line in lines:
        if isinstance(line, dict):
            if line['id'] == line_id:
                arrival_split = str.split(line['arrival'], ':')
                departure_split = str.split(line['departure'], ':')
                hour_diff = int(arrival_split[0]) - int(departure_split[0])
                min_diff = int(arrival_split[1]) - int(departure_split[1])
                if min_diff < 0:
                    min_diff += 60
                    hour_diff -= 1
                if min_diff > 60:
                    min_diff -= 60
                    hour_diff += 1

                print('Cesta trva ' + str(hour_diff) + ' hodin a ' + str(min_diff) + ' minut.')


# Testy

# print(add_line())
# print(add_more_lines())
# print(print_line_info(3))
# print(print_lines_by_type())
# print(count_total_price(5))
# print(delete_line(3))
# print(count_travel_time(4))
