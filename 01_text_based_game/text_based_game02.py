__author__ = 'Jaroslav Belza, 433564@mail.muni.cz'

# Tato hra vznikla v rámci prvního úkolu do Programování v geoinformatice. Po obsahové stránce se může (a předpokládám,
# že pravděpodobně bude) jevit stupidně, nicméně v tomto případě více než jindy platí, že účel světí prostředky :)
#
# Druhý úkol nám zadal rozšíření základu o +-5 cyklů a alespoň 1 seznam, tuple a dictionary. Vzhledem k tomu, že už jsem
# while cykly použil ve velkém množství v základu, takže přidávám alespoň for cyklus, přidávám taky seznamy,
# dictionary a poněkud uměle dotvořený tuple (nenapadlo mě žádné místo v aplikaci, kde by es tuple hodil jako
# improvement existující funkcionality).

print("Vítej v textové hře 'Bilbova cesta do Z6'\nJsi připraven? (Zadej A pro začátek, N pro ukončení hry, H pro "
      "zobrazení nápovědy)")

noOptions = ['n', 'N']
yesOptions = ['a', 'A']
helpOptions = ['h', 'H']
validAnswers = {'1': 'první možnost', '2': 'druhou možnost'}
validAnswersKeysTuple = tuple(validAnswers.keys())


# Start sequence
start = input()
while start not in noOptions + yesOptions + helpOptions:
    print('Zkus to ještě jednou')
    start = input()

if start in noOptions:
    print('Tak ahoj příště')
    exit()
elif start in helpOptions:
    print('Toto je textová hra. Pro její hladký průběh je potřeba špetka představivosti. Na určitých místech si můžeš '
          'zvolit vždy některou\nz těchto možností:')
    for key in validAnswers:
        print('- ' + validAnswers[key] + ',')
    print('Tak teď už opravdu jdeme na to :)\n')
else:
    print('Tak tím pádem jdeme na to :)\n')

end = 0
while end == 0:
    # First question
    print("Jednoho krásného dne si náš hrdina Bilbo jako vždy sedí ve fakultním parčíku, když mu znenadání ve schránce "
          "přistává upozornění,\nže se jeho cvičení Programování v geoinformatice přesouvá do učebny Z6. Už o tomto "
          "tajemném místě zaslechl, ale měl za to, že jde jen\no legendu. 'No to ten den krásně nabývá na zajímavosti, "
          "pomyslí si, čeká mě teď ale bezesporu strastiplná cesta plná nástrah'...\nDo začátku hodiny zbývá 5 minut.\n"
          "1) Ještě máš čas, pohraj si chvíli s prstenem...\n2) Zvedni se Bilbo a vyraž na cestu\n")

    park = input()
    while park not in validAnswers:
        print('Zkus to ještě jednou')
        park = input()

    # First question first answer
    if park == validAnswersKeysTuple[0]:
        print('Zvolil sis ' + validAnswers[validAnswersKeysTuple[0]] + '.')
        print(r"""
             ____
           //----\\
          ||      ||
          ||      ||
           \\____//
             ----""")

        # Second question
        print("\n'Jaký to krásný prsten, kéž bych tak věděl k čemu mi tu bude užitečný'... Do začátku cvičení zbývají "
              "3 minuty. \n1) 3 minuty? Hromada času, zapal si ještě cigaretku\n2) Zvedni se Bilbo a jdi už na to cviko")

        cigar = input()
        while cigar not in validAnswers:
            print('Zkus to ještě jednou')
            cigar = input()

        # Second question first answer
        if cigar == validAnswersKeysTuple[0]:
            print('Zvolil sis ' + validAnswers[validAnswersKeysTuple[0]] + '.')
            print(r""" ___    A
     | |   {*}
     | |  __V__
     |_|o_|%%%|0_
        |       |
        |       |
        |_______|""")

            # Third question v1
            print("\n'No krása, hned mi ty podmínky a cykly půjdou líp od ruky. No ale koukám, že cviko už začíná. "
                  "Má to ještě vůbec cenu?'\n1) 'Samozřejmě, jen už musím vážně pohnout.'\n"
                  "2) 'Nu což, příští týden bude další.'")
            lastCall = input()
            while lastCall not in validAnswers:
                print('Zkus to ještě jednou')
                lastCall = input()

        # Second question second answer & Third question v2
        else:
            print('Zvolil sis ' + validAnswers[validAnswersKeysTuple[1]] + '.')
            print("\n'S miláškem mi ty podmínky a cykly hned půjdou líp od ruky. No ale koukám, že cviko už pomalu"
                  " začíná.' A nebo ne?\nBilbo najednou cítí hroznou potřebu odejít. Jako by ho prsten odlákával "
                  "z fakulty pryč. Jako by jej táhl zpět do\njeho nejoblíbenější Zóny v celém širém Kraji.'\n"
                  "1) Odolat pokušení a rychle se vypravit na cvičení.\n"
                  "2) Poslechnout vábení prstenu a vykašlat se na cvičení.\n")
            lastCall = input()
            while lastCall not in validAnswers:
                print('Zkus to ještě jednou')
                lastCall = input()

        # Third question second answer
        if lastCall == validAnswersKeysTuple[1]:
            print('Zvolil sis ' + validAnswers[validAnswersKeysTuple[1]] + '.')
            print("Bilbo poslechl prsten a vydal se z parčíku od děkanátu směrem doleva ven z fakulty. Před závorou se "
                  "propletl davem \nnaštvaných lidí čekajích na zastávce a jal se čekati na semaforu. Za chvilku se "
                  "dočkal. Už z dálky poznal onen známý zvuk svého oblíbeného místa a co nevidět už před ním stálo\n")
            print(r"""
.   *   ..  . *  *
*  * @()Ooc()*   o  .
    (Q@*0CG*O()  ___
   |\_________/|/ _ \
   |  |  |  |  | / | |
   |  |  |  |  | | | |
   |  |  |  |  | | | |
   |  |  |  |  | | | |
   |  |  |  |  | | | |
   |  |  |  |  | \_| |
   |  |  |  |  | \___/
   |\_|__|__|_/|
    \_________/""")

        # Third question first answer
        else:
            print('Zvolil sis ' + validAnswers[validAnswersKeysTuple[0]] + '.')
            print("Bilbo vyrazil na cviko. Nicméně potkal čaroděje, který jej stejně přenesl do Zóny... :)")
            print(r"""
              _,._      
  .||,       /_ _\\     
 \.`',/      |'L'| |    
 = ,. =      | -,| L    
 / || \    ,-'\"/,'`.   
   ||     ,'   `,,. `.  
   ,|____,' , ,;' \| |  
  (3|\    _/|/'   _| |  
   ||/,-''  | >-'' _,\\ 
   ||'      ==\ ,-'  ,' 
   ||       |  V \ ,|   
   ||       |    |` |   
   ||       |    |   \  
   ||       |    \    \ 
   ||       |     |    \
   ||       |      \_,-'
   ||       |___,,--")_\
   ||         |_|   ccc/
   ||        ccc/       
   ||                   """)

    # First question second answer
    else:
        print('Zvolil sis ' + validAnswers[validAnswersKeysTuple[1]] + '.')
        print("Bilbo vyrazil na cviko. Nicméně potkal čaroděje, který jej stejně přenesl do Zóny... :)")
        print(r"""
                     _,._      
         .||,       /_ _\\     
        \.`',/      |'L'| |    
        = ,. =      | -,| L    
        / || \    ,-'\"/,'`.   
          ||     ,'   `,,. `.  
          ,|____,' , ,;' \| |  
         (3|\    _/|/'   _| |  
          ||/,-''  | >-'' _,\\ 
          ||'      ==\ ,-'  ,' 
          ||       |  V \ ,|   
          ||       |    |` |   
          ||       |    |   \  
          ||       |    \    \ 
          ||       |     |    \
          ||       |      \_,-'
          ||       |___,,--")_\
          ||         |_|   ccc/
          ||        ccc/       
          ||                   """)

    print("A to je vše. Bilbo tak znova prohrál svůj boj s prokrastinací potažmo nadcházejícím alkoholismem. "
          "Ať už vlastní vinou nebo ne.\nTou dobou mu to bylo fuk. O pár let později sotva prolezl "
          "a vyšel ze školy - sice s titulem,\nale bez jakýchkoli znalostí a zkušeností...\n")
    print(r"""
    .   *   ..  . *  *
    *  * @()Ooc()*   o  .
        (Q@*0CG*O()  ___
       |\_________/|/ _ \
       |  |  |  |  | / | |
       |  |  |  |  | | | |
       |  |  |  |  | | | |
       |  |  |  |  | | | |
       |  |  |  |  | | | |
       |  |  |  |  | \_| |
       |  |  |  |  | \___/
       |\_|__|__|_/|
        \_________/""")

    # Play again?
    again = input("Chceš hrát znova? (A pro ano, N pro ne)")
    while again not in yesOptions + noOptions:
        print('Zkus to ještě jednou')
        again = input()

    if again in noOptions:
        end = 1

print('Tak ahoj příště.')
exit()
