"""Navrhněte a naprogramujte systém pro správu televizního programu.
Systém bude ukládat televizní pořady, které budou obsahovat následující informace:
    - název pořadu
    - věkové omezení
    - délku v minutách
    - den vysílání jako datum (buď řetězec ve formátu DD.MM.RRRR nebo použijte knihovnu date)
    - čas začátku vysílání (buď řetězec ve formátu HH:MM nebo použijte knihovnu time)
    - čas konce vysílání (buď řetězec ve formátu HH:MM nebo použijte knihovnu time)
    - informaci zda pořad bude vysílán na placeném nebo neplaceném kanálu

Implementujte funkce, které umožní uživateli přidávat jednotlivé pořady do systému.
!!! Čas konce vysílání uživatel nebude muset zadávat, ale tento čas se vypočítá automaticky z délky pořadu!!!

Následně imlementujte ještě tyto funkce, které uživatel bude vybírat z menu:
    1. Pokud při přidání pořadu už bude existovat stejný pořad, který bude mít stejný název, věkové omezení, délku i den
     a časy vysílání) tak napiště, že takový pořad už existuje a nepřidávejte jej
    2. Zeptejte se uživatele kolik je mu let a poté mu vypište jen pořady, na které se může dívat podle věkového omezení
    3. Po zadání data vypište uživateli všechny pořady, které se v daný den vysílají
    4. Vypište uživateli na dotaz jen pořady, které budou na placeném nebo neplaceném kanálu
    5. Přidejte funkci pro možnost vymazání pořadu ze seznamu podle zadaného názvu. Pokud takový pořad nebude existovat
    vypište, že daný pořad nebyl nalezen a tak ho nebylo možné vymazat
    6. Nechte uživatele zadat čas ve formátu (HH:MM) a zjistěte, které pořady se zrovna vysílají. Ty které se vysílají
    vypište
"""
import time

schedule = [{'name': 'Zpravy', 'age': 0, 'length': 60, 'date': '10.10.2019', 'start': '19:00',
            'end': '20:00', 'isFree': True},
            {'name': 'Vecernicek pro dospele', 'age': 18, 'length': 30, 'date': '10.10.2019', 'start': '19:54',
            'end': '20:24', 'isFree': False}]


def programme_check(new):
    for current in schedule:
        if isinstance(new, dict):
            if current['name'] == new['name']:
                if current['age'] == new['age'] and current['length'] and new['length'] and current['date'] and new['date'] and current['start'] == new['start']:
                    return False
    return True


# 1 Pridani poradu
def add_programme():
    new = {
        'name': input('Zadejte nazev poradu: '),
        'age': int(input('Zadejte pozadovany vek: ')),
        'length': int(input('Zadejte delku v minutach: ')),
        'date': input('Zadejte datum vysilani ve formatu DD.MM.YYYY: '),
        'start': input('Zadejte čas zacatku ve formatu HH:MM: ')}

    if programme_check(new):
        # IS_FREE
        is_free = input('Je porad na placenem kanale (A pro ano, N pro ne)')

        # Validace inputu
        while is_free not in ['A', 'a', 'N', 'n']:
            is_free = input('Je porad na placenem kanale (A pro ano, N pro ne)')

        # Prevod na bool
        if is_free in ['A', 'a']:
            new['isFree'] = True
        else:
            new['isFree'] = False

        # END
        start_split = str.split(new['start'], ':')

        end_hour = int(start_split[0])
        end_minute = int(start_split[1]) + new['length']

        while end_minute >= 60:
            end_hour += 1
            end_minute -= 60

        # Neresim kdyz je cas konce uz v nasledujicim dni...

        new['end'] = str(end_hour) + ':' + str(end_minute)

        schedule.append(new)
        print(new)
        return 0

    else:
        print('Takovy porad jiz v seznamu existuje')
        return 1


# 2 Vypis na zaklade veku
def print_age_ok():
    user_age = input('Zadejte svuj vek: ')
    ok_programmes = []
    for prog in schedule:
        if isinstance(prog, dict):
            if int(user_age) >= prog['age']:
                ok_programmes.append(prog)
    print(ok_programmes)


# 3 Vypis na zaklade data
def print_date():
    date = input('Zadejte datum pro ktere chcete porady vypsat: ')
    today = []
    for prog in schedule:
        if isinstance(prog, dict):
            if prog['date'] == date:
                today.append(prog)
    print(today)


# 4 Vypis placene/neplacene
def print_channel():
    channel_type = input('Vypsat programy podle kanalu - zadejte N pro vypis neplacenych, P pro vypis placenych')
    while channel_type not in ['N', 'n', 'P', 'p']:
        channel_type = input('Vypsat programy podle kanalu - zadejte N pro vypis neplacenych, P pro vypis placenych')
    free = []
    not_free = []
    for prog in schedule:
        if isinstance(prog, dict):
            if prog['isFree']:
                free.append(prog)
            else:
                not_free.append(prog)

    if channel_type in ['N', 'n']:
        print(free)
    else:
        print(not_free)


# 5 Mazani poradu
def delete_programme():
    to_delete = input('Zadejte nazev poradu ktery chcete vymazat: ')
    for prog in schedule:
        if isinstance(prog, dict):
            if prog['name'] == to_delete:
                schedule.remove(prog)
                print(schedule)
                return 0
    print('Porad nebyl nalezen, proto jej nejde smazat')


# 6 Prave hraje
def now_playing():
    now = input('Zadejte aktualni cas ve formatu HH:MM: ')

    current = time.strptime(now, '%H:%M')
    playing = []
    for prog in schedule:
        if isinstance(prog, dict):
            if time.strptime(prog['start'], '%H:%M') <= current <= time.strptime(prog['end'], '%H:%M'):
                playing.append(prog)
    print(playing)


# TESTY
# add_programme()
# print_age_ok()
# print_date()
# print_channel()
# delete_programme()
# now_playing()
