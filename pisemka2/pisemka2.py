

"""
Resenim teto pisemky bude zpracovat 3 dopisy (letter1, letter2 a letter3) od 3 deti, ktere pisou jeziskovi a nakoupit veci z dopisu.
V kazdem dopise je objekt, ktery je mozne koupit na eshopu.
Vytvorte strukturu eshop -> zbozi pomoci trid (class). Trida Shop bude obsahovat vlastnosti nazev a seznam zbozi (objekt tridy Commodity).
Trida Commodity bude obsahovat
nazev, cena a pocet_polozek.
Po vytvoreni struktury vytvorte jednotlive objekty nasledovne:
    Obchod:
        name: Alza
        zbozi:
        [    name: hodinky s vodotryskem
             price: 150
             no_items: 5,

             name: pocitac
             price: 20000
             no_items: 2,

             name: ponik
             price: 50000
             no_items: 1
        ]
        name: CZC
        zbozi:
        [    name: playstation
             price: 9000
             no_items: 5,

             name: Dobroty Ladi Hrusky
             price: 10
             no_items: 100,

             name: ponik
             price: 70000
             no_items: 1
        ]
Nakonec tedy budete mit dva objekty typu Eshop a kazdy z obchodu bude mit pole 3 objektu Commodity
Pro tridy definujte funkce __str__ a __eq__ (rovnost obchodu bude v pripade stejneho nazvu a rovnost zbozi bude v pripade stejneho nazvu i ceny

Cilem je nakoupit jeziskovi v eshopech vsechny veci, o ktere si deti napsaly.
Jezisek ma omezeny rozpocet (promenna money), ale muze si vzit uver od banky (promenna bank).
Projdete dopisy deti a najdete v dopisech veci, ktere mate koupit. Veci jsou oddelene - (pomlckou) a jsou vzdy na konci vet.
V dopisech od deti jsou gramaticke chyby, takze bude potreba hledat varianty s i/y nebo ceske prepisy (pro plejstejsn budete hledat playstation). Pokuste se vytvorit co nejvice univerzalni reseni
Tyto veci se pote pokuste najit v eshopech a "koupit". Pri koupeni musite odecist jednu polozku z poctu dostupnych polozek na eshopu a odecist penize jeziska.
Preferujte nejlevnejsi moznost.
Pokud jezisek nebude mit na danou vec dost penez, bude si muset pujcit od banky (odectete penize z banky).
Nakonec programu vytvorite formatovany vypis do konzole, ktery bude obsahovat informace o vsech nakoupenych polozkach (obchod, cena).
Dale bude obsahovat kolik jeziskovy zbylo penez a kolik si pujcil od banky a o kolik vic zaplati na urocich (pujcene penize zvyseni o urok (rate)).

SHRNUTI:
1. Vytvorte strukturu trid a vytvorte objekty.
2. Zpracujete dopisy od deti (zohlednete gramaticke chyby a ceske tvary)
3. Vyhledate darky z dopisu na eshopu
4. Koupite nejlevnejsi darky na eshopech a odectete pocet polozek v obchode
5. Pokud jeziskovi nevyjdou penize tak si pujcite od banky
6. Vypisete informace o nakupech a jeziskovych financich

ZPUSOB IMPLEMENTACE JE NA VAS. SAMOTNE RESENI (FUNKCE) BUDETE MUSET VYMYSLET SAMI.
"""


class Shop:
    def __init__(self, name: str, commodities: list):
        self.name = name
        self.commodities = commodities

    def __str__(self):
        return 'Obchod - {}.\nSeznam zbozi na sklade - {}'.format(self.name, self.print_commodities())

    def __eq__(self, other):
        return self.name == other.name

    def print_commodities(self):
        for commodity in self.commodities:
            print(commodity)

    def sell_commodity(self, commodity_name):
        for commodity in self.commodities:
            if commodity_name.name == commodity.name:
                if commodity.no_items > 0:
                    commodity.no_items -= 1
                else:
                    print('Zbozi neni na sklade!')


class Commodity:
    def __init__(self, name: str, price: float, no_items: int):
        self.name = name
        self.price = price
        self.no_items = no_items

    def __str__(self):
        return '[Nazev: {}; Cena: {}; Pocet kusu na sklade: {}]'.format(self.name, self.price, self.no_items)

    def __eq__(self, other):
        return self.name == other.name and self.price == other.price


def init():
    alza = Shop('Alza', list([
        Commodity('hodinky s vodotryskem', 150, 5),
        Commodity('pocitac', 20000, 2),
        Commodity('ponik', 50000, 1)
    ]))

    czc = Shop('CZC', list([
        Commodity('playstation', 9000, 5),
        Commodity('Dobroty Ladi Hrusky', 10, 100),
        Commodity('ponik', 70000, 1)
    ]))

    bank = 1000000
    rate = 19.9
    money = 30000

    letter1 = """Myly jezisku, na vanoce bych si pral -hodinky s vodotriskem"""
    letter2 = """Na vanoce bich moc chtel -plejstejsn"""
    letter3 = """Na vanoce bych si prala -ponika"""

    shops = [alza, czc]
    letters = [letter1, letter2, letter3]
    wishes = []
    for letter in letters:
        wishes.append(parse_letter(letter))

    presents = get_presents(wishes, shops, money)

    # Resolve debts
    to_pay_back = 0
    if presents[1] < 0:
        debt = abs(presents[1])
        bank -= debt
        presents[1] += debt
        to_pay_back = debt + debt*(rate/100)

    print("""
    Jezisek nakoupil tyto veci:
        {}
    Konecny stav Jeziskova uctu je {} a bance dluzi i s uroky {}.""".format(presents[0], presents[1], to_pay_back))


def parse_letter(letter: str):
    full = str.split(letter, '-')
    return full[1]


def get_presents(wishes, shops, money):
    # Prohledej shopy
    possible_presents = []
    for wish in wishes:
        # Implementace natvrdo pro toto zadani
        if wish == 'hodinky s vodotriskem':
            wish = 'hodinky s vodotryskem'
        elif wish == 'plejstejsn':
            wish = 'playstation'
        elif wish == 'ponika':
            wish = 'ponik'

        for shop in shops:
            for commodity_new in shop.commodities:
                # Prochazej prani po pismenkach a kdyz se bude shodovat vic jak 70% tak ber jako possible(?)
                # similar_letters = 0
                # if len(commodity.name) >= len(wish):
                #     for letter in range(0, len(wish)-3):
                #         if commodity.name[letter] == wish[letter]:
                #             similar_letters += 1
                #     similarity = similar_letters/len(wish)
                #     # Plejstejsn ma similarity s playstation jen 0.4... :(
                #     if similarity >= 0.7:
                #         commodity.origin = shop.name
                #         possible_presents.append(commodity)

                # Implementace natvrdo pro toto zadani
                if commodity_new.name == wish:
                    commodity_new.origin = shop.name
                    # Pokud uz takovy darek je v seznamu, porovnej cenu
                    possible_presents.append(commodity_new)
                    for current in possible_presents:
                        if current.name == commodity_new.name:
                            # Pokud je nove nalezeny levnejsi, tak stary smaz. - Posledni vec co mi nefunguje :(
                            if current.price > commodity_new.price:
                                possible_presents.remove(current)

    # Nakup nejlevnejsi presenty
    presents_bought = []
    for present in possible_presents:
        for shop in shops:
            if shop.name == present.origin:
                for commodity in shop.commodities:
                    if commodity.name == present.name:
                        shop.sell_commodity(present)
                        money -= commodity.price
                        presents_bought.append(present)

    return [presents_bought, money]


# Beh programu
init()
