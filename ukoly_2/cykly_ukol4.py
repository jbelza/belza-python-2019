seznam = [
    {'brand': 'Ford', 'engine': 1.0, 'price': 300000, 'percent': 0.4},
    {'brand': 'Honda', 'engine': 1.9, 'price': 500000, 'percent': 0.5},
    {'brand': 'Fiat', 'engine': 0.9, 'price': 100000, 'percent': 0.1}
]

for car in seznam:
    car['price'] *= (car['percent'] + 1)

print(seznam)
