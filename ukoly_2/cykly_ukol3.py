cisla = [1, 2, 3, 4, 6, 12, 11, 9, 15, 20, 10, 77, 100]
suda = 0
licha = 0
for i in cisla:
    if i % 2 == 0:
        suda += 1
    else:
        licha += 1

print('Pocet sudych cisel: {}\n'.format(suda))
print('Pocet lichych cisel: {}\n'.format(licha))
