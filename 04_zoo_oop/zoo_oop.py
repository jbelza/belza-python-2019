import ast

# Byli jste pozadani mistni ZOO, abyste pro ne vytvorili primitivní informacni system, ktery bude obsahovat
# informace o zvířatech v ZOO.

# System bude umět přidat a odstranit zvire ze systemu,
# bude mozne zadat krmeni zvirete, zkontrolovat jeho zdravotni stav
# Informace v systemu jsou predstaveny ve forme pole, ktere obsahuje jednotliva zvirata (slovníky), viz promenna animals
# Kdo chce, muze pouzit rovnou funkce, ale neni to nutnost. Ukazeme si rozdil na pristi hodine

# UKOL 4 - Prepiste kod do OOP


class Animal:

    def __init__(self, species: str, name: str, sex: str, fed: bool, healthy: bool):
        self.species = species
        self.name = name
        self.sex = sex
        self.fed = fed
        self.healthy = healthy

    def __str__(self):
        return 'Zvíře: {}, Jméno: {}, Pohlaví: {}, Nakrmeno: {}, Zdravi: {}'.format(self.species, self.name,
                                                                                    self.sex, self.fed, self.healthy)


class Address:

    def __init__(self, street_name: str, street_number: int, city: str, zip: int):
        self.street_name = street_name
        self.street_number = street_number
        self.city = city
        self.zip = zip

    def __str__(self):
        return 'Ulice: {}, Cislo popisne: {}, Mesto: {}, PSC: {}'.format(self.street_name, self.street_number,
                                                                         self.city, self.zip)


class Zoo:

    def __init__(self, animals: list, address: Address):
        self.animals = animals
        self.address = address

    def __str__(self):
        return 'Zoo se nachazi na adrese: {} {}, {} {} a chova tato zvirata: {}'\
            .format(self.address.street_name, self.address.street_number, self.address.zip, self.address.zip,
                    self.print_animals())

    # 2: Nakrmeni zvirat: pokud nektere ze zvirat bude mit fed == False, tak vypiste do konzole,
    # ze dane zvire bylo nakrmeno a nastavte hodnotu fed = True
    def feed_animals(self):
        for animal in self.animals:
            if not animal.fed:
                animal.fed = True
                print('Zvíře {} se jménem {} bylo nakrmeno.'.format(animal.species, animal.name))

    # 3: vyleceni zvirat: obdobne jako u 2
    def heal_animals(self):
        for animal in self.animals:
            if not animal.healthy:
                animal.healthy = True
                print('Zvíře {} se jménem {} bylo vyléčeno.'.format(animal.species, animal.name))

    # 4: nalezeni zvirat co maji hlad: prochazejte pole a pro kazde zvire zkontrolujte zda nema hlad.
    # Pokud bude mit hlad, tak do konzole vypiste informace o zviratech co maji hlad vcetne celkoveho poctu zvirat
    # co maji hlad (napr. Celkovy pocet hladovych zvirat: 2)
    def find_hungry(self):
        count = 0
        for animal in self.animals:
            if not animal.fed:
                print('Hladovějící zvíře {} se jménem {}.'.format(animal.species, animal.name))
                count += 1
        print('Celkový počet hladovějících zvířat: {}'.format(count))

    # 5: obdobne jako u 4, akorat pro klíč healthy
    def find_ill(self):
        count = 0
        for animal in self.animals:
            if not animal.healthy:
                print('Nemocné zvíře {} se jménem {}.'.format(animal.species, animal.name))
                count += 1
        print('Celkový počet nemocných zvířat: {}'.format(count))

    # 6: Vypište nějak pěkne zformátovaný seznam všech zvířat v ZOO
    # nevypisujte jen slovníky ve formě {'species': 'eagle'...}, ale vypiste zvirata v nejake kultivovanejsi forme
    def print_animals(self):
        for animal in self.animals:
            print(animal)

    def add_animal(self):
        species = input('Zadejte druh: \n')
        name = input('Zadejte jméno: \n')
        sex = input('Zadejte pohlaví: \n')
        fed = ast.literal_eval(input('Zadejte zda je zvíře nakrmeno: \n'))  # zadavejte True nebo False
        healthy = ast.literal_eval(input('Zadejte zda je zvíře zdravé: \n'))  # zadavejte True nebo False
        self.animals.append(Animal(species, name, sex, fed, healthy))

    def remove_animal(self, species: str, name: str):
        for animal in self.animals:
            if animal.species == species and animal.name == name:
                self.animals.remove(animal)
                print('Zvire smazano')
                return
        print('Zvire nenalezeno')


# Beh progamu
def run():
    # Testovaci seznam zvirat
    animals = list([
        Animal('wolf', 'Boris', 'male', True, True),
        Animal('wolf', 'Sable', 'female', False, True),
        Animal('elephant', 'Suki', 'female', True, False),
        Animal('eagle', 'Comet', 'male', False, False),
        Animal('panda', 'Zelda', 'female', True, True)
    ])
    # Testovaci adresa
    address = Address('U Zoologicke zahrady', 46, 'Brno', 63500)
    zoo = Zoo(animals, address)

    print('Vítejte v informačním systému ZOO')
    while True:
        answer = int(input('Vyberte jednu z možností:\n'
                           '1: Přidat nové zvíře\n'
                           '2: Nakrmit zvířata\n'
                           '3: Vylečit zvířata\n'
                           '4: Najít zvířata co mají hlad\n'  # fed == False
                           '5: Nají zvířata co jsou nemocná\n'  # healthy == False
                           '6: Vypsat všechna zvířata\n'
                           '7: Smazat zvire\n'
                           '8: Ukončit program\n'))

        if answer == 1:
            zoo.add_animal()
        elif answer == 2:
            zoo.feed_animals()
        elif answer == 3:
            zoo.heal_animals()
        elif answer == 4:
            zoo.find_hungry()
        elif answer == 5:
            zoo.find_ill()
        elif answer == 6:
            zoo.print_animals()
        elif answer == 7:
            species = input('Zadejte druh zvirete: ')
            name = input('Zadejte jmeno zvirete: ')
            zoo.remove_animal(species, name)
        elif answer == 8:
            print('Ukončování programu...')
            break


# Init
run()